import numpy as np
import matplotlib.pyplot as plt
import random
import math
from numpy import mean

count = 1000
nine_count = count * 9

def normalized_rand():
    return math.sqrt(14) * (random.random() - 0.5)

rands = [ normalized_rand() for _ in range(0, nine_count) ]

whiteNoise = []

for j in range(0, count):
    sum = 0.0
    for i in range(0, 9):
        sum += rands[9 * j + i]

    whiteNoise.append(sum/3)

x = [ i for i in range(0, count) ]

plt.title('Значения белого шума')
plt.xlabel('Номер')
plt.ylabel('Значения')

plt.plot(x, whiteNoise)

# Histogramm
plt.figure()

columns = 15

plt.title('Гистограмма белого шума')
plt.xlabel('Значения')
plt.ylabel('Количество')
plt.hist(whiteNoise, columns, rwidth=0.75)

# Корреляционная функция белого шума
plt.figure()

T = 1000
Q = int(T/10)
N = len(whiteNoise)
m = mean(whiteNoise)

out = []
for q in range(0, Q + 1):
    sum = 0
    for i in range(0, N - Q):
        sum += (whiteNoise[i] - m) * ( whiteNoise[i + q] - m )
    out.append(sum/(N-Q))

plt.plot([i for i in range(0, Q + 1)], out)

plt.title('Корреляционная функция белого шума')
plt.xlabel('X')
plt.ylabel('Y')

# color white
plt.figure()

T = 50
k = math.sqrt(2 * T)

colorNoise = [0]

for n in range(1, len(whiteNoise)):
    colorNoise.append( ((T - 1)/T) * colorNoise[n - 1] + k/T * whiteNoise[n])

plt.plot(x, colorNoise)

# Histogramm color
plt.figure()

columns = 15

plt.title('Гистограмма окрашенного сигнала')
plt.xlabel('Значения')
plt.ylabel('Количество')
plt.hist(colorNoise, columns, rwidth=0.75)



# Корреляционная функция цветного шума
plt.figure()

T = 1000
Q = int(T/10)
N = len(colorNoise)
m = mean(colorNoise)

out = []
for q in range(0, Q + 1):
    sum = 0
    for i in range(0, N - Q):
        sum += (colorNoise[i] - m) * ( colorNoise[i + q] - m )
    out.append(sum/(N-Q))

plt.plot([i for i in range(0, Q + 1)], out)

plt.title('Корреляционная функция окрашенного сигнала')
plt.xlabel('X')
plt.ylabel('Y')








plt.show()